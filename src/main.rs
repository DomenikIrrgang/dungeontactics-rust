mod setup;
mod debug;

#[path = "./unit/unit.rs"]
mod unit;

#[path = "./unit/units/player.rs"]
mod player;

#[path = "./unit/model.rs"]
mod model;

#[path = "./camera.rs"]
mod camera;

#[path = "./tilemap.rs"]
mod tilemap;

//#[path = "./tilemap/tilemap_plugin.rs"]
//mod tilemap;

use bevy::prelude::*;
use debug::DebugPlugin;
use setup::SetupPlugin;
use unit::UnitPlugin;
use tilemap::TileMapPlugin;
use crate::camera::CameraPlugin;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(SetupPlugin)
        .add_plugin(CameraPlugin)
        .add_plugin(DebugPlugin)
        .add_plugin(UnitPlugin)
        .add_plugin(TileMapPlugin)
        .add_plugin(player::PlayerPlugin)
        .run();
}
