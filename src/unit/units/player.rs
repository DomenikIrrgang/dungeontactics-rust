use bevy::prelude::*;

use crate::unit::{
    Unit,
    UnitName,
    UnitModel,
    UnitAnimation,
    UnitSpeed
};

static PLAYER_MODEL: &str = "Fairy";

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {

    fn build(&self, app: &mut App) {
        app.add_startup_system(spawn_player)
            .add_system(movement);
    }

}

#[derive(Component)]
pub struct Player;

fn spawn_player(mut commands: Commands) {
    commands.spawn().insert(Player)
        .insert(Unit)
        .insert(UnitName(String::from("Suu")))
        .insert(UnitModel {
            name: String::from(PLAYER_MODEL)
        })
        .insert(UnitAnimation {
            timer: Timer::from_seconds(0.1, true),
            name: String::from("Idle"),
            index: 0,
        }).insert(UnitSpeed(70.0));
    commands.spawn().insert(Unit)
        .insert(UnitName(String::from("Suu")))
        .insert(UnitModel {
            name: String::from("Ghost")
        })
        .insert(UnitAnimation {
            timer: Timer::from_seconds(0.1, true),
            name: String::from("Idle"),
            index: 0,
        }).insert(UnitSpeed(70.0));
}

fn movement(
    time: Res<Time>,
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&Unit, &UnitSpeed, &mut Transform), With<Player>>) {
    for (_, speed, mut transform)  in query.iter_mut() {
        if keyboard_input.pressed(KeyCode::A) {
            transform.translation.x -= speed.0 * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::D) {
            transform.translation.x += speed.0 * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::W) {
            transform.translation.y += speed.0 * time.delta_seconds();
        }
        if keyboard_input.pressed(KeyCode::S) {
            transform.translation.y -= speed.0 * time.delta_seconds();
        }
    }
}