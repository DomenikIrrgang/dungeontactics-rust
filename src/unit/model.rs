use std::collections::HashMap;
use bevy::prelude::*;

pub struct Models(pub HashMap<String, Model>);

#[derive(Component)]
pub struct Model {
    pub texture: Texture,
    pub animation_set: AnimationSet
}

#[derive(Component)]
pub struct Texture(pub Handle<Image>);

#[derive(Component)]
pub struct Animation {
    pub indices: Vec<(usize, f32)>,
}

#[derive(Component)]
pub struct AnimationSet {
    pub animations: HashMap<String, Animation>,
    pub height: f32,
    pub width: f32,
    pub columns: usize,
    pub rows: usize
}

pub fn load(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.insert_resource(Models(HashMap::from([
        (String::from("Fairy"), Model {
            texture: Texture(asset_server.load("fairy.png")),
            animation_set: AnimationSet {
                animations: HashMap::from([
                    (String::from("Idle"), Animation {
                        indices: vec![(0, 0.1), (1, 0.1), (2, 0.1)],
                    })
                ]),
                width: 32.0,
                height: 32.0,
                columns: 3,
                rows: 1
            }
        }),
        (String::from("Ghost"), Model {
            texture: Texture(asset_server.load("ghost.png")),
            animation_set: AnimationSet {
                animations: HashMap::from([
                    (String::from("Idle"), Animation {
                        indices: vec![(0, 0.2), (1, 0.2), (2, 0.2), (3, 0.2)],
                    })
                ]),
                width: 128.0 / 4.0,
                height: 40.0,
                columns: 4,
                rows: 1
            }
        })
    ])));
}

pub fn get_texture_atlas(model: &Model) -> TextureAtlas {
    return TextureAtlas::from_grid(
        model.texture.0.clone(),
        Vec2::new(model.animation_set.width, model.animation_set.height),
        model.animation_set.columns as usize,
        model.animation_set.rows as usize);
}