use std::time::Duration;

use bevy::prelude::*;
use bevy::app::StartupStage;

use crate::model::{
    Models,
    load,
    get_texture_atlas
};

pub struct UnitPlugin;

impl Plugin for UnitPlugin {
    
    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(StartupStage::PreStartup, load)
            .add_startup_system_to_stage(StartupStage::PostStartup, insert_unit_models)
            .add_system(animate_unit);
    }

}

#[derive(Component)]
pub struct Unit;

#[derive(Component, Deref, DerefMut)]
struct AnimationTimer(Timer);

#[derive(Component)]
pub struct UnitName(pub String);

#[derive(Component)]
pub struct UnitAnimation {
    pub timer: Timer,
    pub name: String,
    pub index: usize
}

#[derive(Component)]
pub struct UnitModel {
    pub name: String
}

#[derive(Component)]
pub struct UnitSpeed(pub f32);

fn animate_unit(
    time: Res<Time>,
    models: Res<Models>,
    mut query: Query<(
        &Unit,
        &UnitModel,
        &mut UnitAnimation,
        &mut TextureAtlasSprite,
    )>,
) {
    for (_, model, mut animation, mut sprite) in query.iter_mut() {
        animation.timer.tick(time.delta());
        if animation.timer.just_finished() {
            animation.index = (animation.index + 1) % (models.0[&model.name].animation_set.animations[&animation.name].indices.len());
            let (index, frame_time) = models.0[&model.name].animation_set.animations[&animation.name].indices[animation.index];
            sprite.index = index;
            animation.timer.set_duration(Duration::from_secs_f32(frame_time));
        }
    }
}

fn insert_unit_models(
    mut commands: Commands,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    models: Res<Models>,
    mut query: Query<(Entity, &UnitModel)>
) {
    for (entity, unit_model) in query.iter_mut() {
        let model = models.0.get(&String::from(&unit_model.name)).unwrap();
        commands.entity(entity).insert_bundle(SpriteSheetBundle {
            texture_atlas: texture_atlases.add(get_texture_atlas(model)),
            transform: Transform::from_xyz(0.0, 0.0, 1.0),
            ..default()
        });
    }
}