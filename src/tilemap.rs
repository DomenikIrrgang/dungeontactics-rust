use std::fmt::Debug;
use std::collections::HashSet;

use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use bevy::render::render_resource::TextureUsages;
use rand::prelude::*;

#[derive(Default, Component)]
struct LastUpdate {
    value: f64,
}

#[derive(Component)]
struct CurrentColor(u16);

const CHUNK_SIZE: ChunkSize = ChunkSize(18, 10);
const MAP_SIZE: MapSize = MapSize(2, 2);
const TILE_SIZE_X: f32 = 16.0;
const TILE_SIZE_Y: f32 = 16.0;

pub struct TileMapPlugin;

impl Plugin for TileMapPlugin {

    fn build(&self, app: &mut App) {
        app.add_plugin(TilemapPlugin)
            .add_startup_system(setup_dynamic_map)
            .add_system(set_texture_filters_to_nearest)
            .add_system(random);
    }

}

fn update_map(
    time: ResMut<Time>,
    mut commands: Commands,
    mut query: Query<&mut LastUpdate>,
    mut map_query: MapQuery,
) {
    let current_time = time.seconds_since_startup();
    for mut last_update in query.iter_mut() {
        if (current_time - last_update.value) > 1.0 {
            //map_query.despawn_layer_tiles(&mut commands, 0u16, 0u16);
            println!("updating tile");
            update_random_tile(&mut commands, &mut map_query);
            last_update.value = current_time;
        }
    }
}

fn random(
    time: ResMut<Time>,
    mut query: Query<(&mut Tile, &TileParent, &mut LastUpdate)>,
    mut chunk_query: Query<&mut Chunk>,
) {
    let current_time = time.seconds_since_startup();
    let mut random = thread_rng();
    let mut chunks = HashSet::new();
    for (mut tile, tile_parent, mut last_update) in query.iter_mut() {
        println!("iterting random..");
        if (current_time - last_update.value) > 0.05 {
            println!("updating...");
            tile.texture_index = random.gen_range(0..6);
            last_update.value = current_time;
            chunks.insert(tile_parent.chunk);
        }
    }

    for chunk_entity in chunks.drain() {
        if let Ok(mut chunk) = chunk_query.get_mut(chunk_entity) {
            chunk.needs_remesh = true;
        }
    }
}

fn update_random_tile(commands: &mut Commands, map_query: &mut MapQuery) {
    let mut random = thread_rng();
    let texture_index = random.gen_range(0..255);
    let tile_position = TilePos(random.gen_range(0..16), random.gen_range(0..16));
    println!("TextureIndex: {} TilePosition: {} {}", texture_index, tile_position.0, tile_position.1);
    let results = map_query.set_tile(
        commands,
        tile_position,
        Tile {
            texture_index,
            ..Default::default()
        },
        0u16,
        0u16,
    );
    println!("IsError: {}", results.is_err());
}

pub fn set_texture_filters_to_nearest(
    mut texture_events: EventReader<AssetEvent<Image>>,
    mut textures: ResMut<Assets<Image>>,
) {
    // quick and dirty, run this for all textures anytime a texture is created.
    for event in texture_events.iter() {
        match event {
            AssetEvent::Created { handle } => {
                if let Some(mut texture) = textures.get_mut(handle) {
                    texture.texture_descriptor.usage = TextureUsages::TEXTURE_BINDING
                        | TextureUsages::COPY_SRC
                        | TextureUsages::COPY_DST;
                }
            }
            _ => (),
        }
    }
}

fn update_tile_map(time: ResMut<Time>,
    mut commands: Commands,
    mut query: Query<&mut LastUpdate>,
    mut map_query: MapQuery) {
    let mut random = thread_rng();

    for _ in 0..100 {
        let position = TilePos(random.gen_range(0..16), random.gen_range(0..16));
        // Ignore errors for demo sake.
        let _ = map_query.set_tile(
            &mut commands,
            position,
            Tile {
                texture_index: 0,
                ..Default::default()
            },
            0u16,
            0u16,
        );
        map_query.notify_chunk_for_tile(position, 0u16, 0u16);
    }
}

fn setup_tilemap(mut commands: Commands, asset_server: Res<AssetServer>, mut map_query: MapQuery) {
    let texture_handle = asset_server.load("plains.png");

    // Create map entity and component:
    let map_entity = commands.spawn().id();
    let mut map = Map::new(0u16, map_entity);

    // Creates a new layer builder with a layer entity.
    let (mut layer_builder, _) = LayerBuilder::new(
        &mut commands,
        LayerSettings::new(
            MAP_SIZE,
            CHUNK_SIZE,
            TileSize(TILE_SIZE_X, TILE_SIZE_Y),
            TextureSize(256.0, 256.0),
        ),
        0u16,
        0u16,
    );

    let texture_width: u16 = 256;
    let texture_height: u16 = 256;
    let tile_count_width = texture_width / 16;
    let tile_count_height = texture_height / 16;

    layer_builder.set_all(TileBundle::default());
    
    for y in 0..tile_count_width {
        for x in 0..tile_count_height {
            println!("TextureIndex: {} x: {}, y: {}", x * y + x, x, y);
            layer_builder.set_tile(TilePos(x as u32, (tile_count_height - y) as u32), Tile {
                texture_index: (tile_count_width) * y + x,
                ..Default::default()
            }.into());
        }
    }

    // Builds the layer.
    // Note: Once this is called you can no longer edit the layer until a hard sync in bevy.
    let layer_entity = map_query.build_layer(&mut commands, layer_builder, texture_handle);

    // Required to keep track of layers for a map internally.
    map.add_layer(&mut commands, 0u16, layer_entity);

    // Spawn Map
    // Required in order to use map_query to retrieve layers/tiles.
    commands
        .entity(map_entity)
        .insert(map)
        .insert(Transform::from_xyz(-128.0, -128.0, -0.0))
        .insert(GlobalTransform::default());
}

fn setup_dynamic_map(mut commands: Commands, asset_server: Res<AssetServer>, mut map_query: MapQuery) {
    let texture_handle = asset_server.load("plains.png");

    let map_entity = commands.spawn().id();
    let mut map = Map::new(0u16, map_entity);

    let (mut layer_builder, layer_entity) = LayerBuilder::<TileBundle>::new(
        &mut commands,
        LayerSettings::new(
            MAP_SIZE,
            CHUNK_SIZE,
            TileSize(TILE_SIZE_X, TILE_SIZE_Y),
            TextureSize(96.0, 16.0),
        ),
        0u16,
        0u16,
    );
    layer_builder.set_all(TileBundle::default());
    layer_builder.set_tile(TilePos(1, 1), TileBundle {
        tile: Tile {
            texture_index: 15,
            ..Default::default()
        },
        ..Default::default()
    });

    map_query.build_layer(&mut commands, layer_builder, texture_handle);

    commands.entity(layer_entity).insert(LastUpdate::default());
    // Required to keep track of layers for a map internally.
    map.add_layer(&mut commands, 0u16, layer_entity);

    // Spawn Map
    // Required in order to use map_query to retrieve layers/tiles.
    commands
        .entity(map_entity)
        .insert(map)
        .insert(Transform::from_xyz(
            (-(MAP_SIZE.0 as f32) * (CHUNK_SIZE.0 as f32) * TILE_SIZE_X) / 2.0,
            (-(MAP_SIZE.1 as f32) * (CHUNK_SIZE.1 as f32) * TILE_SIZE_Y) / 2.0,
            0.0))
        .insert(GlobalTransform::default());
}