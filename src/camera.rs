use std::ops::Mul;

use bevy::prelude::*;
use crate::player::Player;

#[derive(Component)]
pub enum CameraMode {
    FollowPlayer(f32),
    FixateOnPlayer
}

pub struct CameraPlugin;

impl Plugin for CameraPlugin {

    fn build(&self, app: &mut App) {
        app.add_startup_system(init_camera)
            .add_system(update_camera)
            .add_system(player_camera_control);
    }

}

#[derive(Component)]
pub struct WorldCamera;

fn init_camera(mut commands: Commands) {
    let mut camera = OrthographicCameraBundle::new_2d();
    camera.orthographic_projection.scale = 0.3;
    commands.spawn_bundle(camera)
        .insert(CameraMode::FollowPlayer(50.0))
        .insert(WorldCamera);
}

fn update_camera(
    mut camera_query: Query<(&mut Transform, &CameraMode), With<Camera>>,
    player_query: Query<&Transform, (With<Player>, Without<Camera>)>
) {
    let player_transform = player_query.single();
    for (mut camera_transform, camera_mode) in camera_query.iter_mut() {
        match camera_mode {
            CameraMode::FollowPlayer(max_distance) => {
                let distance = camera_transform.translation.truncate().distance(player_transform.translation.truncate());
                if distance >= *max_distance {
                    let directional_vector = player_transform.translation.truncate() - camera_transform.translation.truncate();
                    let new_camera_position = player_transform.translation.truncate() - directional_vector.normalize().mul(*max_distance);
                    camera_transform.translation.x = new_camera_position.x;
                    camera_transform.translation.y = new_camera_position.y;
                }
            },
            CameraMode::FixateOnPlayer => {
                camera_transform.translation.x = player_transform.translation.x;
                camera_transform.translation.y = player_transform.translation.y;
            }
        }
    }  
}

fn player_camera_control(kb: Res<Input<KeyCode>>, time: Res<Time>, mut query: Query<&mut OrthographicProjection, With<WorldCamera>>) {
    let dist = 0.5 * time.delta().as_secs_f32();

    for mut projection in query.iter_mut() {
        let mut log_scale = projection.scale.ln();

        if kb.pressed(KeyCode::Up) {
            log_scale -= dist;
        }
        if kb.pressed(KeyCode::Down) {
            log_scale += dist;
        }

        projection.scale = log_scale.exp();
    }
}