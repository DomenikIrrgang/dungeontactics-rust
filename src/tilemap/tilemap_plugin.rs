use bevy::{prelude::*, render::render_resource::TextureUsages};
use rand::prelude::*;

pub struct TileMapPlugin;

#[derive(Component)]
pub struct Tile {
    index: usize
}

#[derive(Component)]
pub struct TileMap {
    tile_size: Vec2,
    texture_atlas: TextureAtlas
}

impl Plugin for TileMapPlugin {

    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(StartupStage::PreStartup, init_default_tile_map)
            .add_startup_system(add_tile)
            .add_system(set_texture_filters_to_nearest);
    }

}

fn init_default_tile_map(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands.spawn().insert(create_tilemap(
        asset_server.load("plains.png"),
        20,
        20,
        Vec2::new(32.0, 32.0)
    ));
    println!("Created default tilemap");
}

fn create_tilemap(image_handle: Handle<Image>, columns: usize, rows: usize, tile_size: Vec2) -> TileMap {
    return TileMap {
        texture_atlas: TextureAtlas::from_grid(image_handle, tile_size, columns, rows),
        tile_size
    }
}

fn add_tile(mut commands: Commands, mut query: Query<(Entity, &TileMap)>, mut texture_atlases: ResMut<Assets<TextureAtlas>>,) {
    let height = 32;
    let width = 32;
    for (_, tile_map) in query.iter_mut() {
        for x in 0..height {
            for y in 0..width {
                commands.spawn().insert_bundle(SpriteSheetBundle {
                    texture_atlas: texture_atlases.add(tile_map.texture_atlas.clone()),
                    transform: Transform::from_xyz(32.0 * (x as f32), 32.0 * (y as f32), 0.0),
                    ..default()
                }).insert(Tile {
                    index: 0
                });
            }
        }
    }
}

fn change_tiles_to_random_index(mut tiles: Query<&mut TextureAtlasSprite, With<Tile>>) {
    let mut random = thread_rng();
    for mut tile in tiles.iter_mut() {
        tile.index = random.gen_range(0..100);
    }
}

pub fn set_texture_filters_to_nearest(
    mut texture_events: EventReader<AssetEvent<Image>>,
    mut textures: ResMut<Assets<Image>>,
) {
    // quick and dirty, run this for all textures anytime a texture is created.
    for event in texture_events.iter() {
        match event {
            AssetEvent::Created { handle } => {
                if let Some(mut texture) = textures.get_mut(handle) {
                    texture.texture_descriptor.usage = TextureUsages::TEXTURE_BINDING
                        | TextureUsages::COPY_SRC
                        | TextureUsages::COPY_DST;
                }
            }
            _ => (),
        }
    }
}