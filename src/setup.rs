use bevy::prelude::*;
use bevy::window::{ WindowDescriptor, PresentMode };

pub struct SetupPlugin;

pub struct ArenaDimensions {
    pub x: f32,
    pub y: f32,
}

impl Plugin for SetupPlugin {

    fn build(&self, app: &mut App) {
        app.add_startup_system(dimensions)
            .insert_resource(WindowDescriptor {
                present_mode: PresentMode::Immediate,
                ..Default::default()
            })
            .add_startup_system(background_color)
            .add_startup_system(title);
    }

}

fn title(mut windows: ResMut<Windows>) {
   windows.primary_mut().set_title(String::from("DungeonTactics"));
}

fn dimensions(mut commands: Commands) {
    commands.insert_resource(ArenaDimensions {
        x: 800.0,
        y: 600.0
    })
}

fn background_color(mut commands: Commands) {
    commands.insert_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
}