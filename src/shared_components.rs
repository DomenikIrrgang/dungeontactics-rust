use bevy::prelude::Component;

#[derive(Component)]
pub struct Name(pub String);