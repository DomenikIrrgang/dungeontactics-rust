use bevy::prelude::*;
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::{
    diagnostic::{Diagnostics}
};

pub struct DebugPlugin;

impl Plugin for DebugPlugin {

    fn build(&self, app: &mut App) {
        app.add_plugin(FrameTimeDiagnosticsPlugin::default())
            .add_plugin(LogDiagnosticsPlugin::default())
            .add_startup_system(setup)
            .add_startup_system(setup_fps_counter)
            .add_system(text_update_system);
    }

}

#[derive(Component)]
struct FpsText;

fn setup_fps_counter(mut commands: Commands, asset_server: Res<AssetServer>) {
    commands
        .spawn_bundle(TextBundle {
            style: Style {
                align_self: AlignSelf::FlexEnd,
                ..default()
            },
            text: Text {
                sections: vec![
                    TextSection {
                        value: String::from("FPS: "),
                        style: TextStyle {
                            font: asset_server.load("fonts/font.ttf"),
                            font_size: 16.0,
                            color: Color::WHITE,
                        },
                    },
                    TextSection {
                        value: String::from(""),
                        style: TextStyle {
                            font: asset_server.load("fonts/font.ttf"),
                            font_size: 16.0,
                            color: Color::GOLD,
                        },
                    },
                ],
                ..default()
            },
            ..default()
        })
        .insert(FpsText);
}

fn setup(mut commands: Commands) {
    commands.spawn_bundle(UiCameraBundle::default());
}

fn text_update_system(diagnostics: Res<Diagnostics>, mut query: Query<&mut Text, With<FpsText>>) {
    for mut text in query.iter_mut() {
        if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
            if let Some(average) = fps.average() {
                text.sections[1].value = format!("{:.2}", average);
                if average > 80.0 {
                    text.sections[1].style.color = Color::GREEN;
                } else {
                    text.sections[1].style.color = Color::RED;
                }
            }
        }
    }
}